
package management_file;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Alex Martin
 */
public class SearchForFilesInDirectory 
{
    private File directory;
    private File[] fileList;
    private String searchTerm;
    private List searchResults = new ArrayList<>();
    
    public SearchForFilesInDirectory()
    {
        
    }
    /**
     * retruns a list of filenames containing the search term from the directory path
     * @param directoryPath
     * @param searchTerm
     * @return List
     */
    public List getSearchResults(String directoryPath, String searchTerm)
    {
        setDirectory(directoryPath);
        setSearchTerm(searchTerm);
        setFileList();
        setSearchResults();
        return searchResults;
    }
    /**
     * Creates list of all files in a directory
     * @param directoryPath
     * @return List
     */
    public List getAllFilesInDirectory(String directoryPath)
    {
        List result = new ArrayList<>();
        try (Stream<Path> walk = Files.walk(Paths.get(directoryPath))) 
        {
            result = walk.filter(Files::isRegularFile).map(x -> x.toString())
                    .collect(Collectors.toList());
            result.forEach(System.out::println);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return result;
    }
    private void setSearchResults()
    {
        for (File file: fileList)
        {
            if(file.getName().contains(searchTerm))
            {
                searchResults.add(file.getName());
            }
        }
    }
    private void setSearchTerm(String searchTerm)
    {
        this.searchTerm = searchTerm;
    }
    private void setFileList()
    {
        this.fileList = directory.listFiles();
    }
    private void setDirectory(String directoryPath)
    {
        this.directory = new File(directoryPath);
    }
}
