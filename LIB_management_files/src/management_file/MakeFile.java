package management_file;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Simple library to make a file. ONLY tested with .txt and .csv
 * @author Alex Martin
 * @version 1
 */
public class MakeFile 
{
    /**
     * Create a text file from List. directory MUST be path e.g. "/temp/".
     * FileName must include type e.g. "aTextFile.txt"
     * @param fileContents - List
     * @param fileName
     * @param directory 
     */
    public static void makeFile 
        (List fileContents, String fileName, String directory)
    {
        fileMaker(fileContents, fileName, directory);
    }
    /**
     * Create a text file from LIST. directory MUST be path e.g. "/temp/"
     * FileName must include type e.g. "aTextFile.txt"
     * @param fileContents - List
     * @param fileName - String
     * @param directory - String
     */
    public static void makeFile 
        (String[] fileContents, String fileName, String directory)
    {
        fileMaker(fileContents, fileName, directory);
    }
    /**
     * Method to create an empty file.
     * Parameter fileNameAndDirectory must be full
     * e.g. /temp/anotherfolder/test.txt
     * @param fileNameAndDirectory 
     */
    public static void makeFile (String fileNameAndDirectory)
    {
        fileMaker(fileNameAndDirectory);
    }
            /**
     * Create a text file form String[]. directory MUST be path e.g. "/temp/"
     * FileName must include type e.g. "aTextFile.txt"
     * @param fileContents - String
     * @param fileName
     * @param directory 
     */
    public static void makeFile 
        (String fileContents, String fileName, String directory)
    {
        fileMaker(fileContents, fileName, directory);
    }
    /**
     * Create a text file form List. directory MUST be path e.g. "/temp/"
     * FileName must include type e.g. "aTextFile.txt"
     * @param fileContents - List
     * @param fileName
     * @param directory
     */
    public static void makeFileReturnBool 
        (List fileContents, String fileName, String directory)
    {
        fileMaker(fileContents, fileName, directory);
    }
    /**
     * Create a text file form String[]. directory MUST be path e.g. "/temp/"
     * FileName must include type e.g. "aTextFile.txt"
     * @param fileContents - String[]
     * @param fileName
     * @param directory
     */
    public static void makeFileReturnBool 
        (String[] fileContents, String fileName, String directory)
    {
       fileMaker(fileContents, fileName, directory);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    private static void fileMaker
        (List fileContents, String fileName, String directory)
    {
        try 
        {
            File file = new File(directory + fileName );
            try (BufferedWriter output = new BufferedWriter(new FileWriter(file))) {
                for(int x = 0; x <fileContents.size();x++)
                {
                    output.write(fileContents.get(x).toString());
                    output.newLine();
                }
            }
        }
        catch ( IOException e ) 
        {
            e.printStackTrace();
        }
    }
    private static void fileMaker
        (String[] fileContents, String fileName, String directory)
    {
        try 
        {
            File file = new File(directory + fileName );
            try (BufferedWriter output = new BufferedWriter(new FileWriter(file))) {
                for(int x = 0; x <fileContents.length;x++)
                {
                    output.write(fileContents[x]);
                    output.newLine();
                }
            }
        }
        catch ( IOException e ) 
        {
            e.printStackTrace();
        }
    }
    private static void fileMaker
        (String fileContents, String fileName, String directory)
    {
        try 
        {
            File file = new File(directory + fileName );
            try (BufferedWriter output = new BufferedWriter(new FileWriter(file))) {
                output.write(fileContents);
                output.newLine();
            }
        }
        catch ( IOException e ) 
        {
            e.printStackTrace();
        }
    }
        private static void fileMaker(String fileNameAndDirectory)
    {
        try 
        {
            File file = new File(fileNameAndDirectory );
            BufferedWriter output = new BufferedWriter(new FileWriter(file));
            output.close();
        }
        catch ( IOException e ) 
        {
            e.printStackTrace();
        }
    }
}