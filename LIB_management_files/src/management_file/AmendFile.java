package management_file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Simple file to amend .txt and .csv files
 * @author Alex Martin
 * @version 1.2
 * v1.2 added method to get return messages fro amending a file
 * v1.1 added function to delete a file
 */
public class AmendFile 
{
    /**
     * Appends a string starting on the next line to a .txt file.
     * directoryPath MUST be full path and file name:
     * e.g. /tmp/afolder/afile.txt
     * @param directoryPath
     * @param stringText 
     */
    public static void appendStringToFile(String directoryPath, String stringText)
    {
        try 
        {
            String text = "\r\n" + stringText;
            Files.write(Paths.get(directoryPath), text.getBytes(), StandardOpenOption.APPEND);
        }
        catch (IOException e) 
        {
            System.out.println("START ERROR JavaFileManagement.FileManagement.amendFile");
            System.out.println("Exception: " + e);
            System.out.println("directoryPath: " + directoryPath);
            System.out.println("stringText: " + stringText);
            System.out.println("END ERROR JavaFileManagement.FileManagement.amendFile");
        }
    }
    /**
     * Appends a string starting on the next line to a .txt file.
     * directoryPath MUST be full path and file name:
     * e.g. /tmp/afolder/afile.txt
     * Returns "SUCCESS" if amendment fulfilled
     * Returns error message if unsuccessful.
     * Error message format:
     * "ERROR JavaFileManagement.FileManagement.amendFile" followed by "###"
     * "Exception: " + exception followed by "###"
     * "directoryPath: " + directoryPath followed by "###"
     * "stringText: " + stringText followed by "###"
     * "END ERROR"
     * @param directoryPath
     * @param stringText
     * @return String
     */
    public static String appendStringToFileReturnsMessage(String directoryPath, String stringText)
    {
        try 
        {
            String text = "\r\n" + stringText;
            Files.write(Paths.get(directoryPath), text.getBytes(), StandardOpenOption.APPEND);
            return "SUCCESS";
        }
        catch (IOException e) 
        {
            String delimiter = "###";
            String tempStr1 = "ERROR JavaFileManagement.FileManagement.amendFile";
            String tempStr2 ="Exception: " + e;
            String tempStr3 ="directoryPath: " + directoryPath;
            String tempStr4 ="stringText: " + stringText;
            String tempStr5 ="END ERROR";
            return tempStr1 + delimiter + tempStr2 + delimiter + tempStr3 + delimiter + tempStr4 + delimiter + tempStr5 + delimiter;
        }
        
    }
    /**
     * Remove the whole line from a txt file with the exact match from lineContent String
     * @param directoryPath
     * @param stringText 
     */
    public static void removeLine(String directoryPath, String stringText)
    {
        try 
        {
            File file = new File(directoryPath);
            List<String> out = Files.lines(file.toPath())
                    .filter(line -> !line.contains(stringText))
                    .collect(Collectors.toList());
            Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        }
        catch (IOException ex)
        {
            System.out.println("START ERROR JavaFileManagement.FileManagement.removeLine");
            System.out.println("Exception: " + ex);
            System.out.println("directoryPath: " + directoryPath);
            System.out.println("stringText: " + stringText);
            System.out.println("END ERROR JavaFileManagement.FileManagement.removeLine");
        }
    }
    /**
     * Remove the whole line from a txt file with the exact match from lineContent String
     * * Returns "SUCCESS" if amendment fulfilled
     * Returns error message if unsuccessful.Error message format:
 "ERROR JavaFileManagement.FileManagement.amendFile" followed by "###"
 "Exception: " + exception followed by "###"
 "directoryPath: " + directoryPath followed by "###"
 "stringText: " + stringText followed by "###"
 "END ERROR"
     * @param directoryPath
     * @param stringText 
     * @return  String
     */
    public static String removeLineReturnMessage(String directoryPath, String stringText)
    {
        try 
        {
            File file = new File(directoryPath);
            List<String> out = Files.lines(file.toPath())
                    .filter(line -> !line.contains(stringText))
                    .collect(Collectors.toList());
            Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
            return "SUCCESS";
        }
        catch (IOException ex)
        {
            String delimiter = "###";
            String tempStr1 = "ERROR JavaFileManagement.FileManagement.amendFile";
            String tempStr2 ="Exception: " + ex;
            String tempStr3 ="directoryPath: " + directoryPath;
            String tempStr4 ="stringText: " + stringText;
            String tempStr5 ="END ERROR";
            return tempStr1 + delimiter + tempStr2 + delimiter + tempStr3 + delimiter + tempStr4 + delimiter + tempStr5 + delimiter;
        }
    }
    /**
     * Method to remove the contents of a file.
     * Parameter directoryPath must be full path and file name
     * e.g. /tempfolder/anotherfolder/file.txt
     * @param directoryPath 
     */
    public static void removeFileContents(String directoryPath)
    {
        File file = new File(directoryPath);
        file.delete();
        MakeFile.makeFile(directoryPath);
    }
    /**
     * removes a file
     * Must be full path with full name e.g. /temp/temp/tempfile.txt
     * @param filePath - String
     */
    public static void removeFile(String filePath)
    {
        File file = new File(filePath);
        file.delete();
    }
}