package management_file;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Collator;
import java.util.Collection;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Alex Martin
 * @version 0.2
 */
public class FileSearch 
{
    /**
     * Returns an alphabetically ordered collection of file names in a directory.
     * If there are no files then the null is returned.
     * @param directoryPath
     * @return Collection
     */
    public static Collection getListOfFileNames(String directoryPath)
    {
        Collection<String> listOfFileNames = new TreeSet<>(Collator.getInstance());
        File file = new File(directoryPath);
        String fileList[] = file.list();
        for(int count = 0; count < fileList.length; count++)
        {
            listOfFileNames.add(fileList[count]);
        }
        return listOfFileNames;
    }
    /**
     * Returns an alphabetically ordered collection of file names in a directory 
     * where some or all of the searchTerm is contained in the filename, 
     * including file extension.
     * @param directoryPath
     * @param searchTerm
     * @return 
     */
    public static Collection getListOfFileNames(String directoryPath, String searchTerm)
    {
        Collection<String> listOfFileNames = new TreeSet<>(Collator.getInstance());
        File file = new File(directoryPath);
        String fileList[] = file.list();
        for(int count = 0; count < fileList.length; count++)
        {
            if(fileList[count].contains(searchTerm))
            {
                listOfFileNames.add(fileList[count]);
            }
        }
        return listOfFileNames;
    }
}
